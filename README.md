# amws-scala - Scala Client for the Amazon Marketplace Web Service

![Sonatype Nexus (Releases)](https://img.shields.io/nexus/r/com.wegtam/amws-scala-core_2.13?server=https%3A%2F%2Foss.sonatype.org)

This software provides a simple scala client for the Amazon Marketplace 
Web Service. It is intended to not parse returned data like reports but
to easy the building and execution of queries. The actual returned data
is subject to frequent changes therefore it should be parsed in your
application.

The documentation for the AMWS is available at: https://developer.amazonservices.com/

Regarding report data, information is more difficult to find but maybe the 
following link can be a good starting point:
https://sellercentral.amazon.com/gp/help/external/200537120

Development is sponsored by [Wegtam GmbH](https://www.wegtam.com).

**ATTENTION!** Currently highly experimental and no guarantee for api stability!

## Getting started

Just add the dependency to your build configuration:

```
libraryDependencies += "com.wegtam" %% "amws-scala-core" % VERSION
```

### Choosing the HTTP client backend

Currently the following sub modules provide access via libraries as the name indicates:

- `amws-scala-akka` via [akka-http](https://akka.io/)
- `amws-scala-http4s` via [http4s-client](https://http4s.org/)
- `amws-scala-sttp` via [sttp](https://sttp.softwaremill.com/)


Just include the desired dependency like this:

```
libraryDependencies += "com.wegtam" %% "amws-scala-sttp" % VERSION
```

#### Dependencies

The required dependencies are expected to be provided by your build configuration
to avoid pulling in versions you might not want to use.

## Development

For development and testing you need to install [sbt](http://www.scala-sbt.org/).
Please see [CONTRIBUTING.md](CONTRIBUTING.md) for details how to to contribute
to the project.
