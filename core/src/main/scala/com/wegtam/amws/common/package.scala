/*
 * Copyright (c) 2017 Contributors as noted in the AUTHORS.md file
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.wegtam.amws

import eu.timepit.refined._
import eu.timepit.refined.api._
import eu.timepit.refined.boolean._
import eu.timepit.refined.cats._
import eu.timepit.refined.collection._
import eu.timepit.refined.string._

package object common {
  type ApiVersion = String Refined MatchesRegex[W.`"^\\\\d{4}-\\\\d{2}-\\\\d{2}$"`.T]
  object ApiVersion extends RefinedTypeOps[ApiVersion, String] with CatsRefinedTypeOpsSyntax

  type EndpointURI       = String
  type ParameterName     = String
  type ParameterValue    = String
  type RequestParameters = Map[ParameterName, ParameterValue]

  type ReportId = String Refined (NonEmpty And Trimmed)
  object ReportId extends RefinedTypeOps[ReportId, String] with CatsRefinedTypeOpsSyntax

  type ReportRequestId = String Refined (NonEmpty And Trimmed)
  object ReportRequestId extends RefinedTypeOps[ReportRequestId, String] with CatsRefinedTypeOpsSyntax

  type SellerID = String Refined (NonEmpty And Trimmed)
  object SellerID extends RefinedTypeOps[SellerID, String] with CatsRefinedTypeOpsSyntax

}
