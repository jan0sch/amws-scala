/*
 * Copyright (c) 2017 Contributors as noted in the AUTHORS.md file
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.wegtam.amws.network

import java.net.URI

/**
  * A base for implementing http client providers.
  *
  * @tparam M A higher kinded type which wraps the return value.
  */
trait HttpClientProvider[M[_]] {

  /**
    * Send a request to the AMWS API and return the response.
    *
    * The current implementation expects a base URI to which the request is
    * performed via a POST request. The request body will be taken from the
    * data of the given payload. The request will be send with the content
    * type `application/x-www-form-urlencoded`.
    *
    * @param url     A url which is used for the request.
    * @param payload The payload for the request which will be send as the request body.
    * @return Either an error message or the response from the API.
    */
  def send(url: URI)(payload: AmwsRequestPayload): M[Either[AmwsError, AmwsResponse]]

}
