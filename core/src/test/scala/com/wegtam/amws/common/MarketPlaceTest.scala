/*
 * Copyright (c) 2017 Contributors as noted in the AUTHORS.md file
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.wegtam.amws.common

import com.wegtam.amws.common.MarketPlace._

import org.scalatest.matchers.must.Matchers
import org.scalatest.wordspec.AnyWordSpec
import org.scalatestplus.scalacheck.ScalaCheckPropertyChecks

class MarketPlaceTest extends AnyWordSpec with Matchers with ScalaCheckPropertyChecks {
  private final val expectedIds = Table(
    ("Marketplace", "ID"),
    (BR, "A2Q3Y263D00KWC"),
    (CA, "A2EUQ1WTGCTBG2"),
    (CN, "AAHKV2X7AFYLW"),
    (DE, "A1PA6795UKMFR9"),
    (ES, "A1RKKUPIHCS9HS"),
    (FR, "A13V1IB3VIYZZH"),
    (IN, "A21TJRUUN4KGV"),
    (IT, "APJ6JRA9NG5V4"),
    (JP, "A1VC38T7YXB528"),
    (MX, "A1AM78C64UM0Y8"),
    (UK, "A1F83G8C2ARO7P"),
    (US, "ATVPDKIKX0DER")
  )
  private final val expectedRegion = Table(
    ("Marketplace", "Region"),
    (BR, Region.Brazil),
    (CA, Region.NorthAmerica),
    (CN, Region.China),
    (DE, Region.Europe),
    (ES, Region.Europe),
    (FR, Region.Europe),
    (IN, Region.India),
    (IT, Region.Europe),
    (JP, Region.Japan),
    (MX, Region.NorthAmerica),
    (UK, Region.Europe),
    (US, Region.NorthAmerica)
  )

  "#value" must {
    "return the correct marketplace id" in {
      forAll(expectedIds) { (mp: MarketPlace, id: ParameterValue) =>
        mp.value mustEqual id
      }
    }
  }

  "#region" must {
    "return the correct region of the marketplace" in {
      forAll(expectedRegion) { (mp: MarketPlace, r: Region) =>
        mp.region must be(r)
      }
    }
  }
}
