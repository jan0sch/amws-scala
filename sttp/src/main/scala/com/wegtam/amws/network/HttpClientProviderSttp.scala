/*
 * Copyright (c) 2017 Contributors as noted in the AUTHORS.md file
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.wegtam.amws.network

import java.net.URI

import cats.effect._
import cats.syntax.all._
import fs2.Stream
import sttp.client._
import sttp.model._

/**
  * The implementation of a `com.wegtam.amws.network.HttpClientProvider` using sttp.
  *
  * @param backend A backend supported by sttp (@see https://sttp.softwaremill.com/en/latest/backends/summary.html).
  */
class HttpClientProviderSttp[F[_]: ConcurrentEffect]()(implicit
    backend: SttpBackend[F, Stream[F, Byte], NothingT]
) extends HttpClientProvider[F] {

  override def send(url: URI)(payload: AmwsRequestPayload): F[Either[AmwsError, AmwsResponse]] = {
    val req = basicRequest
      .body(payload.data.getOrElse(""))
      .contentType("application/x-www-form-urlencoded")
      .post(Uri(url))
      .response(asStringAlways)
    for {
      response <- req.send()
    } yield response.isSuccess match {
      case false => Left(AmwsError(code = response.code.code, details = Option(response.body)))
      case true  => Right(AmwsResponse(body = response.body))
    }
  }

}
