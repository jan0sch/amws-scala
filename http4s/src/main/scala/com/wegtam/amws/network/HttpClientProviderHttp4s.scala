/*
 * Copyright (c) 2017 Contributors as noted in the AUTHORS.md file
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.wegtam.amws.network

import java.net.URI

import cats.effect._
import cats.syntax.all._
import org.http4s._
import org.http4s.client._
import org.http4s.headers._
import org.http4s.MediaType

/**
  * The implementation of a `com.wegtam.amws.network.HttpClientProvider` using http4s.
  *
  * @param client A http4s client.
  */
class HttpClientProviderHttp4s[F[_]: ConcurrentEffect](client: Client[F]) extends HttpClientProvider[F] {

  /**
    * Perform the given HTTP request and return either the result or an error.
    *
    * @param req An HTTP request.
    * @return The corresponding AmwsResponse.
    */
  protected def performRequest(req: Request[F]): F[Either[AmwsError, AmwsResponse]] =
    client.run(req).use {
      case Status.Successful(r) => r.as[String].map(b => Right(AmwsResponse(body = b)))
      case r                    => r.as[String].map(b => Left(AmwsError(code = r.status.code, details = Option(b))))
    }

  override def send(url: URI)(payload: AmwsRequestPayload): F[Either[AmwsError, AmwsResponse]] = {
    val req = Request[F](
      method = Method.POST,
      uri = Uri.unsafeFromString(url.toASCIIString)
    ).withHeaders(`Content-Type`(MediaType.application.`x-www-form-urlencoded`))
      .withEntity(payload.data.getOrElse(""))
    performRequest(req)
  }
}
