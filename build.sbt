// *****************************************************************************
// Projects
// *****************************************************************************

lazy val amwsScala =
  project
    .in(file("."))
    .aggregate(core, akka, http4s, sttp)
    .settings(settings)
    .settings(
      console := (core / Compile / console).value,
      publish / skip := true,
      Test / console := (core / Test / console).value,
      ThisBuild / Test / parallelExecution := false,
      crossScalaVersions := Nil, // Workaround for "Repository for publishing is not specified." error
      publish := {},
      publishLocal := {},
    )

lazy val core =
  project
    .in(file("core"))
    .enablePlugins(AutomateHeaderPlugin)
    .settings(settings)
    .settings(publishSettings)
    .settings(
      name := "amws-scala-core",
      libraryDependencies ++= Seq(
        library.catsCore,
        library.enumeratum,
        library.enumeratumCats,
        library.refinedCore,
        library.refinedCats,
        library.yaidom,
        library.refinedScalaCheck % Test,
        library.scalaCheck        % Test,
        library.scalaCheckTools   % Test,
        library.scalaTest         % Test,
        library.scalaTestPlus     % Test,
      ),
      Compile / compile / wartremoverWarnings ++= Warts.unsafe.filterNot(_ == Wart.Any)
    )

lazy val akka =
  project
    .in(file("akka"))
    .enablePlugins(AutomateHeaderPlugin)
    .dependsOn(core)
    .settings(settings)
    .settings(publishSettings)
    .settings(
      name := "amws-scala-akka",
      libraryDependencies ++= Seq(
        library.akkaHttp          % Provided,
        library.akkaStream        % Provided,
        library.catsCore          % Provided,
        library.akkaHttpTestkit   % Test,
        library.refinedScalaCheck % Test,
        library.scalaCheck        % Test,
        library.scalaCheckTools   % Test,
        library.scalaTest         % Test,
        library.scalaTestPlus     % Test,
      ),
      Compile / compile / wartremoverWarnings ++= Warts.unsafe.filterNot(_ == Wart.Any)
    )

lazy val http4s =
  project
    .in(file("http4s"))
    .enablePlugins(AutomateHeaderPlugin)
    .dependsOn(core)
    .settings(settings)
    .settings(publishSettings)
    .settings(
      name := "amws-scala-http4s",
      libraryDependencies ++= Seq(
        library.http4sDsl         % Provided,
        library.http4sBlazeClient % Provided,
        library.akkaHttp          % Test,
        library.akkaStream        % Test,
        library.akkaHttpTestkit   % Test,
        library.http4sBlazeServer % Test,
        library.refinedScalaCheck % Test,
        library.scalaCheck        % Test,
        library.scalaCheckTools   % Test,
        library.scalaTest         % Test,
        library.scalaTestPlus     % Test
      ),
      Compile / compile / wartremoverWarnings ++= Warts.unsafe.filterNot(_ == Wart.Any)
    )

lazy val sttp =
  project
    .in(file("sttp"))
    .enablePlugins(AutomateHeaderPlugin)
    .dependsOn(core)
    .settings(settings)
    .settings(publishSettings)
    .settings(
      name := "amws-scala-sttp",
      libraryDependencies ++= Seq(
        library.catsCore          % Provided,
        library.catsEffect        % Provided,
        library.fs2Core           % Provided,
        library.sttpClientCore    % Provided,
        library.akkaHttp          % Test,
        library.akkaStream        % Test,
        library.akkaHttpTestkit   % Test,
        library.refinedScalaCheck % Test,
        library.scalaCheck        % Test,
        library.scalaCheckTools   % Test,
        library.scalaTest         % Test,
        library.scalaTestPlus     % Test,
        library.sttpClientFs2     % Test
      ),
      Compile / compile / wartremoverWarnings ++= Warts.unsafe.filterNot(_ == Wart.Any)
    )

// *****************************************************************************
// Library dependencies
// *****************************************************************************

lazy val library =
  new {
    object Version {
      val akkaHttp        = "10.2.6"
      val akkaStream      = "2.6.15"
      val cats            = "2.7.0"
      val catsEffect      = "2.5.3"
      val enumeratum      = "1.7.0"
      val fs2             = "2.5.9"
      val http4s          = "0.21.26"
      val refined         = "0.9.27"
      val scalaCheck      = "1.15.4"
      val scalaCheckTools = "0.6.0"
      val scalaTest       = "3.2.9"
      val scalaTestPlus   = "3.2.9.0"
      val shapeless       = "2.3.2"
      val sttpClient      = "2.2.9"
      val yaidom          = "1.11.0"
    }
    val akkaHttp             = "com.typesafe.akka"            %% "akka-http"                     % Version.akkaHttp
    val akkaHttpTestkit      = "com.typesafe.akka"            %% "akka-http-testkit"             % Version.akkaHttp
    val akkaStream           = "com.typesafe.akka"            %% "akka-stream"                   % Version.akkaStream
    val catsCore             = "org.typelevel"                %% "cats-core"                     % Version.cats
    val catsEffect           = "org.typelevel"                %% "cats-effect"                   % Version.catsEffect
    val enumeratum           = "com.beachape"                 %% "enumeratum"                    % Version.enumeratum
    val enumeratumCats       = "com.beachape"                 %% "enumeratum-cats"               % Version.enumeratum
    val enumeratumScalaCheck = "com.beachape"                 %% "enumeratum-scalacheck"         % Version.enumeratum
    val fs2Core              = "co.fs2"                       %% "fs2-core"                      % Version.fs2
    val http4sBlazeServer    = "org.http4s"                   %% "http4s-blaze-server"           % Version.http4s
    val http4sBlazeClient    = "org.http4s"                   %% "http4s-blaze-client"           % Version.http4s
    val http4sDsl            = "org.http4s"                   %% "http4s-dsl"                    % Version.http4s
    val refinedCore          = "eu.timepit"                   %% "refined"                       % Version.refined
    val refinedCats          = "eu.timepit"                   %% "refined-cats"                  % Version.refined
    val refinedScalaCheck    = "eu.timepit"                   %% "refined-scalacheck"            % Version.refined
    val scalaCheck           = "org.scalacheck"               %% "scalacheck"                    % Version.scalaCheck
    val scalaCheckTools      = "com.47deg"                    %% "scalacheck-toolbox-datetime"   % Version.scalaCheckTools
    val scalaTest            = "org.scalatest"                %% "scalatest"                     % Version.scalaTest
    val scalaTestPlus        = "org.scalatestplus"            %% "scalacheck-1-15"               % Version.scalaTestPlus
    val shapeless            = "com.chuusai"                  %% "shapeless"                     % Version.shapeless
    val sttpClientCore       = "com.softwaremill.sttp.client" %% "core"                          % Version.sttpClient
    val sttpClientFs2        = "com.softwaremill.sttp.client" %% "async-http-client-backend-fs2" % Version.sttpClient
    val yaidom               = "eu.cdevreeze.yaidom"          %% "yaidom"                        % Version.yaidom
  }

// *****************************************************************************
// Settings
// *****************************************************************************

lazy val settings =
  commonSettings ++
  headerSettings ++
  scalafmtSettings

def compilerSettings(sv: String) =
  CrossVersion.partialVersion(sv) match {
    case Some((2, 13)) =>
      Seq(
        "-deprecation",
        "-explaintypes",
        "-feature",
        "-language:_",
        "-unchecked",
        "-Xcheckinit",
        "-Xfatal-warnings",
        "-Xlint:adapted-args",
        "-Xlint:constant",
        "-Xlint:delayedinit-select",
        "-Xlint:doc-detached",
        "-Xlint:inaccessible",
        "-Xlint:infer-any",
        "-Xlint:missing-interpolator",
        "-Xlint:nullary-unit",
        "-Xlint:option-implicit",
        "-Xlint:package-object-classes",
        "-Xlint:poly-implicit-overload",
        "-Xlint:private-shadow",
        "-Xlint:stars-align",
        "-Xlint:type-parameter-shadow",
        "-Ywarn-dead-code",
        "-Ywarn-extra-implicit",
        "-Ywarn-numeric-widen",
        "-Ywarn-unused:implicits",
        "-Ywarn-unused:imports",
        "-Ywarn-unused:locals",
        "-Ywarn-unused:params",
        "-Ywarn-unused:patvars",
        "-Ywarn-unused:privates",
        "-Ywarn-value-discard",
        "-Ycache-plugin-class-loader:last-modified",
        "-Ycache-macro-class-loader:last-modified",
        "-Ymacro-annotations" // Needed for Simulacrum
      )
    case _ =>
      Seq(
      "-deprecation",
      "-encoding", "UTF-8",
      "-explaintypes",
      "-feature",
      "-language:_",
      "-target:jvm-1.8",
      "-unchecked",
      "-Xcheckinit",
      "-Xfatal-warnings",
      "-Xfuture",
      "-Xlint:adapted-args",
      "-Xlint:by-name-right-associative",
      "-Xlint:constant",
      "-Xlint:delayedinit-select",
      "-Xlint:doc-detached",
      "-Xlint:inaccessible",
      "-Xlint:infer-any",
      "-Xlint:missing-interpolator",
      "-Xlint:nullary-override",
      "-Xlint:nullary-unit",
      "-Xlint:option-implicit",
      "-Xlint:package-object-classes",
      "-Xlint:poly-implicit-overload",
      "-Xlint:private-shadow",
      "-Xlint:stars-align",
      "-Xlint:type-parameter-shadow",
      "-Xlint:unsound-match",
      "-Ydelambdafy:method",
      "-Yno-adapted-args",
      "-Ypartial-unification",
      "-Ywarn-numeric-widen",
      "-Ywarn-unused-import",
      "-Ywarn-value-discard"
    )
  }

lazy val commonSettings =
  Seq(
    ThisBuild / scalaVersion := "2.13.6",
    crossScalaVersions := Seq(scalaVersion.value, "2.12.14"),
    organization := "com.wegtam",
    Compile / packageBin / mappings += (ThisBuild / baseDirectory).value / "LICENSE" -> "LICENSE",
    addCompilerPlugin("com.olegpy"    %% "better-monadic-for" % "0.3.1"),
    addCompilerPlugin("org.typelevel" %% "kind-projector"     % "0.13.0" cross CrossVersion.full),
    scalacOptions ++= compilerSettings(scalaVersion.value),
    scalacOptions ++= {
      if (scalaVersion.value.startsWith("2.11"))
        Seq("-Xmax-classfile-name", "78")
      else
        Seq()
    },
    javacOptions ++= Seq(
      "-encoding", "UTF-8",
      "-source", "1.8",
      "-target", "1.8"
    ),
    autoAPIMappings := true,
    Compile / console / scalacOptions --= Seq(
      "-Xfatal-warnings",
      "-Ywarn-unused-import",
      "-Ywarn-unused:implicits",
      "-Ywarn-unused:imports",
      "-Ywarn-unused:locals",
      "-Ywarn-unused:params",
      "-Ywarn-unused:patvars",
      "-Ywarn-unused:privates"
    ),
    Test / console / scalacOptions --= Seq(
      "-Xfatal-warnings",
      "-Ywarn-unused-import",
      "-Ywarn-unused:implicits",
      "-Ywarn-unused:imports",
      "-Ywarn-unused:locals",
      "-Ywarn-unused:params",
      "-Ywarn-unused:patvars",
      "-Ywarn-unused:privates"
    )
  )

lazy val headerSettings =
  Seq(
    licenses += ("MPL-2.0", url("https://www.mozilla.org/en-US/MPL/2.0/")),
    organizationName := "Contributors as noted in the AUTHORS.md file",
    startYear := Some(2017),
  )

lazy val publishSettings =
  Seq(
    credentials += Credentials(Path.userHome / ".sbt" / "sonatype_credentials"),
    description := "A simple scala client library for the Amazon Marketplace Web Service (AMWS).",
    developers := List(
      Developer(
        "wegtam",
        "Wegtam GmbH",
        "tech@wegtam.com",
        url("https://www.wegtam.com")
      ),
      Developer(
        "jan0sch",
        "Jens Grassel",
        "jens@wegtam.com",
        url("https://www.jan0sch.de")
      )
    ),
    ThisBuild / dynverSeparator := "-",
    ThisBuild / dynverSonatypeSnapshots := true,
    homepage := Option(url("https://codeberg.org/wegtam/amws-scala")),
    pgpSigningKey := Option(sys.env.getOrElse("PUBLISH_SIGNING_KEY", "633EA119CC2D5F249A0F409A002841124A42559E")),
    pomIncludeRepository := (_ => false),
    Test / publishArtifact := false,
    publishMavenStyle := true,
    publishTo := {
      val nexus = "https://oss.sonatype.org/"
      if (isSnapshot.value)
        Some("snapshots".at(nexus + "content/repositories/snapshots"))
      else
        Some("releases".at(nexus + "service/local/staging/deploy/maven2"))
    },
    publish := (publish dependsOn (Test / test)).value,
    scmInfo := Option(ScmInfo(
      url("https://codeberg.org/wegtam/amws-scala"),
      "git@codeberg.org:wegtam/amws-scala.git"
    ))
  )

lazy val scalafmtSettings =
  Seq(
    scalafmtOnCompile := true
  )
